package net.sssubtlety.sss_translate.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

/*
TODO:
    - see about not saving runtime files to disk if they're the same as source translations
    - see about supporting Crowdin branches
    - make main class a separate sub-project
 */

public class SssTranslate extends Thread {
    public static final String NAME = "sss_translate";

    public static final Path RUNTIME_RESOURCES = Path.of("mod_translations");
    public static final Path SOURCE_RESOURCES = Path.of("src", "main", "resources");
    public static final Path MOD_CONFIG = Path.of("config", NAME + ".txt");

    public static final long RECENTLY_DOWNLOADED_PERIOD_MILLIS = TimeUnit.DAYS.toMillis(1);

    public static final String VERBOSE_OPTION = "-v";

    private static final Pattern DOWNLOAD_DISABLED_LINE =
        Pattern.compile("^[ \t]*download[ \t]*[=:][ \t]*(?:false|no)[ \t]*$", CASE_INSENSITIVE);
    private static final Pattern CROWDIN_JSON_PATH =
        Pattern.compile("^(?<crowdinCode>[a-z]{2,4}(?:-[A-Z]{2})?)/(?<fileName>.+\\.json)$");

    private static final Set<String> REGISTERED_MODS;
    private static final Map<String, List<String>> MC_2_CROWDIN_CODES;

    private static Allowed DOWNLOAD = Allowed.UNKNOWN;

    static {
        REGISTERED_MODS = new HashSet<>();

        MC_2_CROWDIN_CODES = Map.ofEntries(
            createEntry("af_za", "af"),
            createEntry("ar_sa", "ar"),
            createEntry("ast_es", "ast"),
            createEntry("az_az", "az"),
            createEntry("ba_ru", "ba"),
            // Bavaria
            // createEntry("bar", "bar"),
            createEntry("be_by", "be"),
            createEntry("bg_bg", "bg"),
            createEntry("br_fr", "br-FR"),
            // Brabantian
            // createEntry("brb", "brb"),
            createEntry("bs_ba", "bs"),
            createEntry("ca_es", "ca"),
            createEntry("cs_cz", "cs"),
            createEntry("cy_gb", "cy"),
            createEntry("da_dk", "da"),
            createEntry("de_at", "de-AT", "de"),
            createEntry("de_ch", "de-CH", "de"),
            createEntry("de_de", "net"),
            createEntry("el_gr", "el"),
            createEntry("en_au", "en-AU", "en-GB", "en-US"),
            createEntry("en_ca", "en-CA", "en-GB", "en-US"),
            createEntry("en_gb", "en-GB", "en-US"),
            createEntry("en_nz", "en-NZ", "en-GB", "en-US"),
            createEntry("en_pt", "en-PT", "en-GB", "en-US"),
            createEntry("en_ud", "en-UD", "en-GB", "en-US"),
            createEntry("en_us", "en-US"),
            // Anglish
            // createEntry("enp", "enp"),
            createEntry("en_ws", "en-WS"),
            createEntry("en_7s", "en-PT"),
            createEntry("eo_uy", "eo"),
            createEntry("es_ar", "es-AR", "es-ES"),
            createEntry("es_cl", "es-CL", "es-ES"),
            createEntry("es_ec", "es-EC", "es-ES"),
            createEntry("es_es", "es-ES"),
            createEntry("es_mx", "es-MX", "es-ES"),
            createEntry("es_uy", "es-UY", "es-ES"),
            createEntry("es_ve", "es-VE", "es-ES"),
            // Andalusian
            createEntry("esan", "esan"),
            createEntry("et_ee", "et"),
            createEntry("eu_es", "eu"),
            createEntry("fa_ir", "fa"),
            createEntry("fi_fi", "fi"),
            createEntry("fil_ph", "fil"),
            createEntry("fo_fo", "fo"),
            createEntry("fr_ca", "fr-CA", "fr"),
            createEntry("fr_fr", "fr"),
            createEntry("fra_de", "fra-DE"),
            createEntry("fy_nl", "fy-NL"),
            createEntry("ga_ie", "ga-IE"),
            createEntry("gd_gb", "gd"),
            createEntry("gl_es", "gl"),
            createEntry("haw_us", "haw"),
            createEntry("he_il", "he"),
            createEntry("hi_in", "hi"),
            createEntry("hr_hr", "hr"),
            createEntry("hu_hu", "hu"),
            createEntry("hy_am", "hy-AM"),
            createEntry("id_id", "id"),
            createEntry("ig_ng", "ig"),
            createEntry("io_en", "ido"),
            createEntry("is_is", "is"),
            // Interslavic
            // createEntry("isv", "isv"),
            createEntry("it_it", "it"),
            createEntry("ja_jp", "ja"),
            createEntry("jbo_en", "jbo"),
            createEntry("ka_ge", "ka"),
            createEntry("kk_kz", "kk"),
            createEntry("kn_in", "kn"),
            createEntry("ko_kr", "ko"),
            // Ripuarian
            // createEntry("ksh", "ksh"),
            createEntry("kw_gb", "kw"),
            createEntry("la_la", "la-LA"),
            createEntry("lb_lu", "lb"),
            createEntry("li_li", "li"),
            createEntry("lol_us", "lol"),
            createEntry("lt_lt", "lt"),
            createEntry("lv_lv", "lv"),
            // Classical Chinese
            // createEntry("lzh", "lzh"),
            createEntry("mi_NZ", "mi"),
            createEntry("mk_mk", "mk"),
            createEntry("mn_mn", "mn"),
            createEntry("ms_my", "ms"),
            createEntry("mt_mt", "mt"),
            createEntry("nds_de", "nds"),
            createEntry("nl_be", "nl-BE", "nl"),
            createEntry("nl_nl", "nl"),
            createEntry("nn_no", "nn-NO", "no"),
            createEntry("no_no", "no", "nb"),
            createEntry("oc_fr", "oc"),
            // Elfdalian
            // createEntry("ovd", "ovd"),
            createEntry("pl_pl", "pl"),
            createEntry("pt_br", "pt-BR", "pt-PT"),
            createEntry("pt_pt", "pt-PT", "pt-BR"),
            createEntry("qya_aa", "qya-AA"),
            createEntry("ro_ro", "ro"),
            // Russian (pre-revolutionary)
            // createEntry("rpr", "rpr"),
            createEntry("ru_ru", "ru"),
            createEntry("se_no", "se"),
            createEntry("sk_sk", "sk"),
            createEntry("sl_si", "sl"),
            createEntry("so_so", "so"),
            createEntry("sq_al", "sq"),
            createEntry("sr_sp", "sr"),
            createEntry("sv_se", "sv-SE"),
            // Upper Saxon German
            // createEntry("sxu", "sxu"),
            // Silesian
            // createEntry("szl", "szl"),
            createEntry("ta_in", "ta"),
            createEntry("th_th", "th"),
            createEntry("tl_ph", "tl"),
            createEntry("tlh_aa", "tlh-AA"),
            createEntry("tr_tr", "tr"),
            createEntry("tt_ru", "tt-RU"),
            createEntry("uk_ua", "uk"),
            createEntry("val_es", "val-ES"),
            createEntry("vec_it", "vec"),
            createEntry("vi_vn", "vi"),
            createEntry("yi_de", "yi"),
            createEntry("yo_ng", "yo"),
            createEntry("zh_cn", "zh-CN", "zh-HK"),
            createEntry("zh_hk", "zh-HK", "zh-CN"),
            createEntry("zh_tw", "zh-TW")
        );
    }

    private static Map.Entry<String, List<String>> createEntry(String key, String... values) {
        return Map.entry(key, List.of(values));
    }

    public static void refreshConfig() {
        DOWNLOAD = Allowed.UNKNOWN;
    }

    public static void downloadSourceTranslations(String projectName) {
        downloadSourceTranslations(projectName, projectName, null, false);
    }

    public static void downloadRuntimeTranslations(String projectName) {
        downloadRuntimeTranslations(projectName, projectName, null, false);
    }

    public static void downloadSourceTranslations(String crowdinProjectName, String minecraftProjectName) {
        downloadSourceTranslations(crowdinProjectName, minecraftProjectName, null, false);
    }

    public static void downloadRuntimeTranslations(String crowdinProjectName, String minecraftProjectName) {
        downloadRuntimeTranslations(crowdinProjectName, minecraftProjectName, null, false);
    }

    public static void downloadSourceTranslations(
        String crowdinProjectName, String minecraftProjectName, boolean verbose
    ) {
        downloadSourceTranslations(crowdinProjectName, minecraftProjectName, null, verbose);
    }

    public static void downloadRuntimeTranslations(
        String crowdinProjectName, String minecraftProjectName, boolean verbose
    ) {
        downloadRuntimeTranslations(crowdinProjectName, minecraftProjectName, null, verbose);
    }

    public static void downloadSourceTranslations(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride
    ) {
        downloadSourceTranslations(crowdinProjectName, minecraftProjectName, sourceFileOverride, false);
    }

    public static void downloadRuntimeTranslations(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride
    ) {
        downloadRuntimeTranslations(crowdinProjectName, minecraftProjectName, sourceFileOverride, false);
    }

    public static void downloadSourceTranslations(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride, boolean verbose
    ) {
        downloadTranslationsAndWait(
            crowdinProjectName, minecraftProjectName, sourceFileOverride, verbose, SOURCE_RESOURCES, null
        );
    }

    public static void downloadRuntimeTranslations(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride, boolean verbose
    ) {
        downloadTranslations(
            crowdinProjectName, minecraftProjectName, sourceFileOverride, verbose, RUNTIME_RESOURCES, MOD_CONFIG
        );
    }

    public static void downloadTranslationsAndWait(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride, boolean verbose,
        Path resources, Path modConfig
    ) {
        downloadTranslations(
            crowdinProjectName, minecraftProjectName, sourceFileOverride, verbose,
            resources, modConfig
        ).ifPresent(thread -> {
            try {
                thread.join(10000);
            } catch (InterruptedException e) {
                System.err.println("Error downloading translations");
                e.printStackTrace();
            }
        });
    }

    static Optional<Thread> downloadTranslations(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride, boolean verbose,
        Path resources, Path modConfig
    ) {
        REGISTERED_MODS.add(minecraftProjectName);
        final boolean mod = modConfig != null;
        if (mod && (!shouldDownload(modConfig) || downloadedRecently(minecraftProjectName, resources)))
            return Optional.empty();

        final var thread = new SssTranslate(
            minecraftProjectName, crowdinProjectName, sourceFileOverride, verbose, resources, mod
        );
        thread.start();

        return Optional.of(thread);
    }

    private static synchronized boolean shouldDownload(Path config) {
        if (DOWNLOAD != Allowed.UNKNOWN) return DOWNLOAD == Allowed.YES;

        final var configFile = config.toFile();

        try {
            configFile.getParentFile().mkdirs();
            try {
                if (configFile.createNewFile()) {
                    try {
                        try (var writer = new FileWriter(configFile)) {
                            writer.write(
                                """
                                    # Set this to false to prevent mod translation downloads
                                    download=true
                                    
                                    """
                            );
                        }
                    } catch (IOException e) {
                        System.err.println("Error writing to file: " + config);
                        e.printStackTrace();
                    }
                } else {
                    try (var reader = new BufferedReader(new FileReader(configFile))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            if (DOWNLOAD_DISABLED_LINE.matcher(line).matches()) {
                                DOWNLOAD = Allowed.NO;
                                return false;
                            }
                        }
                    } catch (IOException e) {
                        System.err.println("Error reading from file: " + config);
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                System.err.println("Error creating file: " + config);
                e.printStackTrace();
            }
        } catch (SecurityException e) {
            System.err.println("Error accessing file: " + config);
            e.printStackTrace();
        }

        DOWNLOAD = Allowed.YES;
        return true;
    }

    public static Set<String> getRegisteredMods() {
        return new HashSet<>(REGISTERED_MODS);
    }

    private static boolean downloadedRecently(String projectName, Path resources) {
        final var timestampFile = getTimestampFile(projectName, resources);
        return timestampFile.exists() &&
            timestampFile.lastModified() > System.currentTimeMillis() - RECENTLY_DOWNLOADED_PERIOD_MILLIS;
    }

    private static void updateTimestamp(String projectName, Path resources) {
        final var timestampFile = getTimestampFile(projectName, resources);
        try {
            timestampFile.getParentFile().mkdirs();
            timestampFile.createNewFile();
        } catch (IOException ex) {
            // bad luck, we'll just check again next time.
        }

        timestampFile.setLastModified(System.currentTimeMillis());
    }

    private static File getTimestampFile(String projectName, Path parent) {
        return parent.resolve(projectName + ".timestamp").toFile();
    }

    public static void main(String[] args) {
        final boolean verbose;
        if (args.length > 0 && args[0].equals(VERBOSE_OPTION)) {
            verbose = true;
            args = Arrays.copyOfRange(args, 1, args.length);
        } else verbose = false;

        switch (args.length) {
            case 1 -> downloadSourceTranslations(args[0], args[0], verbose);
            case 2 -> downloadSourceTranslations(args[0], args[1], verbose);
            case 3 -> downloadSourceTranslations(args[0], args[1], args[2], verbose);
            default -> System.out.printf(
                """
                Usage: java -jar base-<version>.jar [%s] crowdin_project_name [minecraft_project_name] [source_file_override]
                \tDownload translations from crowdin.com/project/<crowdin_project_name>
                \t\tto assets/<minecraft_project_name>/lang
                \t%s enables verbose logging
                \tminecraft_project_name is only necessary if your
                \t\tCrowdin and Minecraft project names are different,
                \t\tor if you're also passing a source_file_override
                \tsource_file_override is only necessary if your Crowdin project
                \t\thas multiple source files
                
                """, VERBOSE_OPTION, VERBOSE_OPTION
            );
        }
    }

    private final String crowdinProjectName;
    private final String minecraftProjectName;
    private final String sourceFileOverride;
    private final boolean verbose;
    private final Path resources;
    private final boolean timestamp;

    private SssTranslate(
        String minecraftProjectName, String crowdinProjectName,
        String sourceFileOverride, boolean verbose, Path resources, boolean timestamp
    ) {
        this.crowdinProjectName = crowdinProjectName;
        this.minecraftProjectName = minecraftProjectName;
        this.sourceFileOverride = sourceFileOverride == null || sourceFileOverride.toLowerCase().endsWith((".json")) ?
            sourceFileOverride :
            sourceFileOverride + ".json";
        this.verbose = verbose;
        this.resources = resources;
        this.timestamp = timestamp;
    }

    @Override
    public void run() {
        try {
            final var translations = getCrowdinTranslations(crowdinProjectName);

            final var assetPath = resources.resolve(Path.of("assets", minecraftProjectName, "lang"));
            final var assetPathString = assetPath.toString();
            assetPath.toFile().mkdirs();

            for (final var entry : MC_2_CROWDIN_CODES.entrySet()) {
                for (final var attemptingSource : entry.getValue()) {
                    final byte[] buffer = translations.get(attemptingSource);
                    if (buffer != null) {
                        final var filePath = Path.of(assetPathString, entry.getKey() + ".json");
                        if (verbose) System.out.println(
                            "Writing " + buffer.length + " bytes from \"" + attemptingSource + "\"" +
                                " to MC file \"" + filePath + "\""
                        );

                        saveBufferToJsonFile(buffer, filePath);
                        break;
                    }
                }
            }

            if (timestamp) updateTimestamp(minecraftProjectName, resources);
        } catch (IOException ex) {
            System.err.println("Error downloading translations");
            ex.printStackTrace();
        }

    }

    private Map<String, byte[]> getCrowdinTranslations(String projectName) throws IOException {
        final Map<String, byte[]> zipContents = new HashMap<>();

        final var urlString = "https://crowdin.com/backend/download/project/" + projectName + ".zip";

        try {
            final var url = new URI(urlString).toURL();

            try (var zipInput = new ZipInputStream(url.openStream())) {
                if (verbose) System.out.println("Trying to download " + url);

                ZipEntry entry;
                while ((entry = zipInput.getNextEntry()) != null) {
                    final var path = entry.getName();
                    final var matcher = CROWDIN_JSON_PATH.matcher(path);
                    if (matcher.matches()) {
                        final var crowdinCode = matcher.group("crowdinCode");
                        final var fileName = matcher.group("fileName");
                        if (sourceFileOverride != null && !sourceFileOverride.equals(fileName)) {
                            if (verbose) System.out.println(
                                "Ignoring \"" + path + "\", we're looking for \"" + sourceFileOverride + "\""
                            );

                            continue;
                        }

                        if (verbose) System.out.println(
                            "Found translation \"" + crowdinCode + "\" for file: " + fileName
                        );

                        if (entry.getSize() > 10_000_000) {
                            // This is mainly a guard against broken files that could exhaust our memory.
                            throw new IOException("File too large: " + entry.getName() + ": " + entry.getSize());
                        }

                        if (zipContents.containsKey(crowdinCode)) {
                            System.err.println("More than one file for " + crowdinCode + ", skipping " + fileName);

                            continue;
                        }

                        zipContents.put(matcher.group(1), getZipStreamContent(zipInput, (int) entry.getSize()));
                    }
                }
            }
        } catch (URISyntaxException e) {
            System.err.println("Project name forms invalid URL: " + urlString);
        }

        return zipContents;
    }

    private byte[] getZipStreamContent(InputStream is, int size) throws IOException {
        final byte[] buf = new byte[size];
        int toRead = size;
        int totalRead = 0, readNow;

        while (toRead > 0) {
            if ((readNow = is.read(buf, totalRead, toRead)) <= 0)
                throw new IOException("premature end of stream");

            totalRead += readNow;
            toRead -= readNow;
        }

        return buf;
    }

    private void saveBufferToJsonFile(byte[] buffer, Path path) {
        final var file = path.toFile();
        try (FileOutputStream stream = new FileOutputStream(file)) {
            stream.write(buffer);
        } catch (IOException e) {
            System.err.println("Failed to write to file: " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }

    private enum Allowed {
        YES,
        NO,
        UNKNOWN
    }
}
