package net.sssubtlety.sss_translate.base

import net.sssubtlety.sss_translate.fixtures.util.IoUtil
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.Path

import static net.sssubtlety.sss_translate.base.SssTranslate.*
import static net.sssubtlety.sss_translate.fixtures.util.Constants.*

class SssTranslateMainTest extends Specification {
    @TempDir
    File workingDir

    def 'download translations'(List<String> args) {
        given:
        final verbose = args[0] == '-v'
        final iSecondProjectName = verbose ? 2 : 1
        final minecraftProjectName = args.size() > iSecondProjectName ?
            args[iSecondProjectName] : args[iSecondProjectName - 1]
        final langDir =
            IoUtil.mkdirs(workingDir, 'src', 'main', 'resources', 'assets', minecraftProjectName, 'lang')

        when:
        final process = exec(args)

        then:
        final output = new String(process.inputStream.readAllBytes())
        final err = new String(process.errorStream.readAllBytes())

        ignore(() -> process.waitFor())
        process.exitValue() == 0

        verbose == output.contains('Trying to download ')
        err.isEmpty()
        langDir.list().length > 0

        where:
        _|| args
        _|| [SAME_PROJECT_NAME]
        _|| [VERBOSE_OPTION, SAME_PROJECT_NAME]
        _|| [CROWDIN_PROJECT_NAME, MINECRAFT_PROJECT_NAME]
        _|| [VERBOSE_OPTION, CROWDIN_PROJECT_NAME, MINECRAFT_PROJECT_NAME]
        _|| [CROWDIN_PROJECT_NAME, MINECRAFT_PROJECT_NAME, SOURCE_FILE]
        _|| [VERBOSE_OPTION, CROWDIN_PROJECT_NAME, MINECRAFT_PROJECT_NAME, SOURCE_FILE]
    }

    Process exec(List<String> args) throws IOException, InterruptedException {
        final String javaBin = Path.of(System.getProperty('java.home'), 'bin', 'java').toString()
        final String classpath = System.getProperty("java.class.path")
        final String className = SssTranslate.class.getName()

        final builder = new ProcessBuilder([
            javaBin,
            "-cp",
            classpath,
            className,
            *args
        ])
        final process = builder.directory(workingDir).start()

        return process
    }

    static void ignore(Runnable runnable) {
        runnable.run()
    }
}
