package net.sssubtlety.sss_translate.base

import net.sssubtlety.sss_translate.fixtures.util.IoUtil
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.Path

import static net.sssubtlety.sss_translate.base.SssTranslate.*
import static net.sssubtlety.sss_translate.fixtures.util.Constants.*

class DownloadTranslationsTest extends Specification {
    @TempDir
    File root

    def 'download fresh translations'(
        Path resources, String crowdinProjectName, String minecraftProjectName,
        String sourceFile, boolean verbose, boolean mod
    ) {
        given:
        refreshConfig()

        final rootPath = root.toPath()
        final fullResourcesPath = rootPath.resolve(resources)
        final langDir = makeLangDir(fullResourcesPath, minecraftProjectName)
        final configDir = rootPath.resolve(MOD_CONFIG)

        when:
        downloadTranslationsAndWait(
            crowdinProjectName, minecraftProjectName, sourceFile, verbose,
            fullResourcesPath, mod ? configDir : null
        )

        then:
        langDir.list().length > 0
        mod == fullResourcesPath.resolve(minecraftProjectName + '.timestamp').toFile().exists()

        where:
        resources           | crowdinProjectName    | minecraftProjectName      | sourceFile    | verbose   | mod
        SOURCE_RESOURCES    | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | SOURCE_FILE   | true      | false
        SOURCE_RESOURCES    | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | null          | true      | false
        SOURCE_RESOURCES    | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | SOURCE_FILE   | false     | false
        SOURCE_RESOURCES    | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | null          | false     | false
        SOURCE_RESOURCES    | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | SOURCE_FILE   | true      | false
        SOURCE_RESOURCES    | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | null          | true      | false
        SOURCE_RESOURCES    | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | SOURCE_FILE   | false     | false
        SOURCE_RESOURCES    | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | null          | false     | false
        RUNTIME_RESOURCES   | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | SOURCE_FILE   | true      | true
        RUNTIME_RESOURCES   | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | null          | true      | true
        RUNTIME_RESOURCES   | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | SOURCE_FILE   | false     | true
        RUNTIME_RESOURCES   | SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | null          | false     | true
        RUNTIME_RESOURCES   | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | SOURCE_FILE   | true      | true
        RUNTIME_RESOURCES   | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | null          | true      | true
        RUNTIME_RESOURCES   | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | SOURCE_FILE   | false     | true
        RUNTIME_RESOURCES   | CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | null          | false     | true
    }

    def 'download runtime translations with existing files'(
        String crowdinProjectName, String minecraftProjectName, boolean recentTimestamp
    ) {
        given:
        refreshConfig()

        final rootPath = root.toPath()
        final fullResourcesPath = rootPath.resolve(RUNTIME_RESOURCES)
        final langDir = makeLangDir(fullResourcesPath, minecraftProjectName)
        final configDir = rootPath.resolve(MOD_CONFIG)

        final timestampFile = IoUtil.createFile(fullResourcesPath, minecraftProjectName + '.timestamp')
        if (!recentTimestamp) assert timestampFile.setLastModified(
            System.currentTimeMillis() - (RECENTLY_DOWNLOADED_PERIOD_MILLIS + 1000)
        )

        IoUtil.createFile(root, MOD_CONFIG.toString()).write("download=$config")

        when:
        final thread = downloadTranslations(
            crowdinProjectName, minecraftProjectName, null, false,
            fullResourcesPath, configDir
        )
        thread.ifPresent { it.join(10000) }

        then:
        thread.isPresent() == (config && !recentTimestamp)
        thread.isPresent() == langDir.list().size() > 0

        where:
        crowdinProjectName    | minecraftProjectName      | recentTimestamp | config
        SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | true            | true
        SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | true            | false
        SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | false           | true
        SAME_PROJECT_NAME     | SAME_PROJECT_NAME         | false           | false
        CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | true            | true
        CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | true            | false
        CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | false           | true
        CROWDIN_PROJECT_NAME  | MINECRAFT_PROJECT_NAME    | false           | false
    }

    File makeLangDir(Path resources, String minecraftProjectName) {
        IoUtil.mkdirs(resources, 'assets', minecraftProjectName, 'lang')
    }
}
