package net.sssubtlety.sss_translate.fixtures.util

class Constants {
    static final SAME_PROJECT_NAME = 'meticulous'
    static final CROWDIN_PROJECT_NAME = 'wither-cage-fix'
    static final MINECRAFT_PROJECT_NAME = 'wither_cage_fix'

    static final SOURCE_FILE = 'en_us.json'
}
