package net.sssubtlety.sss_translate.fixtures.util

import java.nio.file.Path

@SuppressWarnings('unused')
class IoUtil {
    private IoUtil() { }

    static File mkdirs(File first, String... more) throws SecurityException {
        mkdirs(first.toString(), more)
    }

    static File mkdirs(Path first, String...more) throws SecurityException {
        mkdirs(first.toString(), more)
    }

    static File mkdirs(String first, String... more) throws SecurityException {
        final dir = Path.of(first, more).toFile()
        dir.mkdirs()
        return dir
    }

    static File createFile(File parent, String name) throws IOException, RuntimeException {
        createFile(parent.toString(), name)
    }

    static File createFile(Path parent, String name) throws IOException, RuntimeException {
        createFile(parent.toString(), name)
    }

    static File createFile(String parent, String name) throws IOException, RuntimeException {
        final file = new File(parent, name)
        file.getParentFile().mkdirs()
        file.createNewFile()
        return file
    }
}
