package net.sssubtlety.sss_translate.mod;

import java.io.File;
import java.io.InputStream;
import java.util.*;

import net.sssubtlety.sss_translate.base.SssTranslate;
import net.minecraft.resource.*;
import net.minecraft.resource.metadata.ResourceMetadataReader;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import static net.sssubtlety.sss_translate.base.SssTranslate.RUNTIME_RESOURCES;
import static net.sssubtlety.sss_translate.base.SssTranslate.NAME;

/**
 * Code taken from LambdAurora, oral permission on Discord on 2020-10-04
 */
public class SssTranslateResourcePack implements ResourcePack {
    private final Set<String> namespaces = new HashSet<>();

    public SssTranslateResourcePack() {
        for (String mod: SssTranslate.getRegisteredMods())
            add(mod + "/lang");

        add(NAME + "/lang");
    }

    public void add(String resource) {
        tryFromPath(resource).map(Identifier::getNamespace).ifPresent(namespaces::add);
    }

    @Override
    public InputSupplier<InputStream> openRoot(String ... fileName) {
        File file = RUNTIME_RESOURCES.resolve(fileName[0]).toFile();
        return file.exists() ? InputSupplier.create(file.toPath()) : null;
    }

    @Override
    public InputSupplier<InputStream> open(ResourceType type, Identifier id) {
        return this.openRoot(type.getDirectory() + "/" + id.getNamespace() + "/" + id.getPath());
    }

    @Override
    public void findResources(
        ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer consumer
    ) {
        final var files = new File(RUNTIME_RESOURCES + "/assets/" + namespace + "/" + prefix).list();

        if (files == null || files.length == 0) return;

        Arrays.stream(files)
            .map(SssTranslateResourcePack::tryFromPath)
            .filter(Optional::isPresent)
            .flatMap(Optional::stream)
            .forEach(result -> consumer.accept(result, open(type, result)));
    }

    @Override
    public Set<String> getNamespaces(ResourceType type) {
        return new HashSet<>(this.namespaces);
    }

    @Override
    public <T> T parseMetadata(ResourceMetadataReader<T> metaReader) {
        return null;
    }

    @Override
    public ResourcePackInfo getInfo() {
        return new ResourcePackInfo(
            "Sss Translate internal Resource Pack",
            Text.of("Sss Translate internal Resource Pack"),
            ResourcePackSource.BUILTIN,
            Optional.empty()
        );
    }

    @Override
    public void close() { }

    private static Optional<Identifier> tryFromPath(String path) {
        if (path.startsWith("assets/"))
            path = path.substring("assets/".length());

        final var split = path.split("/", 2);
        return split.length == 2 ?
            Optional.ofNullable(Identifier.tryParse(split[0], split[1])) :
            Optional.empty();
    }
}
