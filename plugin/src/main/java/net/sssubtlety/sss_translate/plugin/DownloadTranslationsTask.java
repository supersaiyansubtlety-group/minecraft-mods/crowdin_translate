package net.sssubtlety.sss_translate.plugin;

import net.sssubtlety.sss_translate.base.SssTranslate;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.*;

@UntrackedTask(because = "downloading doesn't work with up-to-date checks")
public abstract class DownloadTranslationsTask extends DefaultTask {
    @Input
    public abstract Property<String> getCrowdinProjectName();

    @Input
    public abstract Property<String> getMinecraftProjectName();

    @Input
    @Optional
    public abstract Property<String> getSourceFileOverride();

    @Input
    public abstract Property<Boolean> getVerbose();

    @OutputDirectory
    public abstract RegularFileProperty getResourcesDir();

    @TaskAction
    public void run() {
        SssTranslate.downloadTranslationsAndWait(
            getCrowdinProjectName().get(),
            getMinecraftProjectName().get(),
            getSourceFileOverride().getOrNull(),
            getVerbose().get(),
            getResourcesDir().get().getAsFile().toPath(),
            null
        );

        this.setDidWork(true);
    }
}
