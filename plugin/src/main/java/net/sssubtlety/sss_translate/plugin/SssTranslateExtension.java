package net.sssubtlety.sss_translate.plugin;

import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Property;

public abstract class SssTranslateExtension {
    public abstract Property<String> getCrowdinProjectName();

    public abstract Property<String> getMinecraftProjectName();

    public abstract Property<String> getSourceFileOverride();

    public abstract Property<Boolean> getVerbose();

    public abstract RegularFileProperty getResourcesDir();
}
