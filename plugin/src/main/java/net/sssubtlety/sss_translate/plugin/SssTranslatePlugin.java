/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sssubtlety.sss_translate.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.jvm.tasks.Jar;

import static net.sssubtlety.sss_translate.base.SssTranslate.SOURCE_RESOURCES;

/**
 *
 * @author gbl
 */
public class SssTranslatePlugin implements Plugin<Project> {
    public static final String EXT = "sssTranslate";
    public static final String DOWNLOAD_SOURCE_TRANSLATIONS_TASK_NAME = "downloadSourceTranslations";

    @Override
    public void apply(Project project) {
        final var ext = project.getExtensions().create(EXT, SssTranslateExtension.class);
        ext.getVerbose().convention(false);
        ext.getMinecraftProjectName().convention(ext.getCrowdinProjectName());
        ext.getResourcesDir().fileProvider(project.provider(() ->
            project.getProjectDir().toPath().resolve(SOURCE_RESOURCES).toFile()
        ));

        final var tasks = project.getTasks();
        tasks.register(DOWNLOAD_SOURCE_TRANSLATIONS_TASK_NAME, DownloadTranslationsTask.class, task -> {
            task.getCrowdinProjectName().set(ext.getCrowdinProjectName());
            task.getMinecraftProjectName().set(ext.getMinecraftProjectName());
            task.getSourceFileOverride().set(ext.getSourceFileOverride());
            task.getVerbose().set(ext.getVerbose());
            task.getResourcesDir().set(ext.getResourcesDir());
            task.mustRunAfter(tasks.withType(Jar.class));
        });
    }
}
