package net.sssubtlety.sss_translate.plugin

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir

import static net.sssubtlety.sss_translate.fixtures.util.IoUtil.createFile
import static org.gradle.api.Project.DEFAULT_BUILD_FILE
import static org.gradle.api.Project.GRADLE_PROPERTIES
import static org.gradle.api.initialization.Settings.DEFAULT_SETTINGS_FILE

abstract class AbstractGradleBuildTest extends Specification {
    @TempDir File projectDir
    File gradleProperties
    File settingsGradle
    File buildGradle

    def setup() {
        gradleProperties = createFile(projectDir, GRADLE_PROPERTIES)
        settingsGradle = createFile(projectDir, DEFAULT_SETTINGS_FILE)
        buildGradle = createFile(projectDir, DEFAULT_BUILD_FILE)
    }

    protected abstract BuildResult build()

    protected abstract BuildResult build(String... args)

    protected final GradleRunner createProjectGradleRunner() {
        return GradleRunner.create().withProjectDir(projectDir)
    }
}
