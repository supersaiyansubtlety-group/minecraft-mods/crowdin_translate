package net.sssubtlety.sss_translate.plugin


import org.gradle.testkit.runner.BuildResult

abstract class GradleBuildTest extends AbstractGradleBuildTest {
    @Override
    BuildResult build() {
        createProjectGradleRunner().build()
    }

    @Override
    BuildResult build(String... args) {
        createProjectGradleRunner()
            .withArguments(args)
            .build()
    }
}
