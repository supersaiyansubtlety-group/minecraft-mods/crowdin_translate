package net.sssubtlety.sss_translate.plugin


import org.gradle.testkit.runner.BuildResult

class GradlePluginBuildTest extends AbstractGradleBuildTest {
    @Override
    BuildResult build() {
        createProjectGradleRunner().withPluginClasspath().build()
    }

    @Override
    BuildResult build(String... args) {
        createProjectGradleRunner().withPluginClasspath().withArguments(args).build()
    }
}
