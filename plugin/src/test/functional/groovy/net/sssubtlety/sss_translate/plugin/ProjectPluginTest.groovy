package net.sssubtlety.sss_translate.plugin

abstract class ProjectPluginTest extends GradlePluginBuildTest {
    def setup() {
        buildGradle << pluginsBlock
    }

    protected abstract String getPluginId()

    protected boolean shouldApply() { true }

    protected Iterable<String> getAdditionalPluginIds() { [] }

    private String getPluginsBlock() {
        final indent = '    '

        final builder = new StringBuilder('plugins {\n')

        builder.append "${indent}id '$pluginId' apply ${shouldApply()}\n"

        for (var id : additionalPluginIds) builder.append "${indent}id '$id'\n"

        builder.append '}\n'

        return builder.toString()
    }
}
