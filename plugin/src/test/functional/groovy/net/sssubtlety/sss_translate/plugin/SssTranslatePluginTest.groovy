package net.sssubtlety.sss_translate.plugin

import net.sssubtlety.sss_translate.fixtures.util.IoUtil
import org.gradle.testkit.runner.BuildResult

import static net.sssubtlety.sss_translate.plugin.SssTranslatePlugin.*
import static net.sssubtlety.sss_translate.fixtures.util.Constants.*

class SssTranslatePluginTest extends ProjectPluginTest {
    @Override
    protected String getPluginId() { 'net.sssubtlety.sss_translate.sss-translate' }

    @Override
    protected Iterable<String> getAdditionalPluginIds() { ['java'] }

    def 'fails without crowdin project name'() {
        when:
        runDownloadSourceTranslations()

        then:
        final RuntimeException e = thrown()
        e.message.contains "property 'crowdinProjectName' doesn't have a configured value."
    }

    def 'configure extension'(
        String crowdinProjectName, String minecraftProjectName, String sourceFileOverride, Boolean verbose
    ) {
        given:
        final langDir =
            IoUtil.mkdirs(projectDir, 'src', 'main', 'resources', 'assets', minecraftProjectName, 'lang')

        when:
        buildGradle <<
            """
            $EXT {
                crowdinProjectName = '$crowdinProjectName'
            """.stripIndent()
        if (minecraftProjectName != null) buildGradle << "\n    minecraftProjectName = '$minecraftProjectName'"
        if (sourceFileOverride != null) buildGradle << "\n    sourceFileOverride = '$sourceFileOverride'"
        if (verbose != null) buildGradle << "\n    verbose = $verbose"
        buildGradle << "\n}"

        final result = runDownloadSourceTranslations()

        then:
        langDir.list().size() > 0
        (verbose != null && verbose) == result.output.contains('Trying to download ')

        where:
        crowdinProjectName      | minecraftProjectName      | sourceFileOverride    | verbose
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | null              | null
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | null              | true
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | null              | false
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | SOURCE_FILE       | null
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | SOURCE_FILE       | true
        SAME_PROJECT_NAME       | SAME_PROJECT_NAME         | SOURCE_FILE       | false
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | null              | null
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | null              | true
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | null              | false
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | SOURCE_FILE       | null
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | SOURCE_FILE       | true
        CROWDIN_PROJECT_NAME    | MINECRAFT_PROJECT_NAME    | SOURCE_FILE       | false
    }

    def 'build can depend on downloadSourceTranslations'() {
        given:
        final mainDir = IoUtil.mkdirs(projectDir, 'src', 'main')
        final langDir = IoUtil.mkdirs(mainDir, 'resources', 'assets', SAME_PROJECT_NAME, 'lang')
        final packageDir = IoUtil.mkdirs(mainDir, 'net', 'sssubtlety')
        IoUtil.createFile(packageDir, 'Empty.java')

        buildGradle <<
            """
            $EXT {
                crowdinProjectName = '$SAME_PROJECT_NAME'
            }
            
            java {
                withSourcesJar()
            }
            """.stripIndent()

        when:
        buildGradle <<
            """
            build {
                dependsOn downloadSourceTranslations
            }
            """.stripIndent()

        build('build')

        then:
        langDir.list().size() > 0
    }

    BuildResult runDownloadSourceTranslations() {
        build(DOWNLOAD_SOURCE_TRANSLATIONS_TASK_NAME)
    }
}
