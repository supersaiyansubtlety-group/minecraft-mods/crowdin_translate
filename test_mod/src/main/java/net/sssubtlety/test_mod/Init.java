package net.sssubtlety.test_mod;

import com.mojang.brigadier.arguments.StringArgumentType;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.minecraft.text.Text;

import static net.minecraft.server.command.CommandManager.*;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        CommandRegistrationCallback.EVENT.register(
            (dispatcher, commandBuildContext, environment) ->
                dispatcher.register(literal("queryTranslation")
                    .then(argument("translationKey", StringArgumentType.greedyString())
                        .executes(context -> {
                            context.getSource().sendFeedback(() ->
                                Text.translatable(StringArgumentType.getString(context, "translationKey")),
                                false
                            );
                            return 1;
                        })
                    )
                )
        );
    }
}
